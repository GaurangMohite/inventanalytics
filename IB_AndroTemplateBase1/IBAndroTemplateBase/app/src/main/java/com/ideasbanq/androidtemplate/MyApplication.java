/*
 * Copyright (c) 2015. IdeasBanq PTE LTD. All rights reserved. This material may not be reproduced, displayed, modified or distributed without the express prior written permission of the copyright holder. For permission, contact Dr Insu Song, inssong@gmail.com.
 */

package com.ideasbanq.androidtemplate;

import android.app.Application;

import com.ideasbanq.androidtemplate.Model.DbHelper;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(formKey = "",
        mailTo = "inssong@gmail.com",
        mode = ReportingInteractionMode.DIALOG,
        resToastText = R.string.crash_toast_text,
        resDialogIcon = R.drawable.ic_launcher,
        resDialogTitle = R.string.crash_dialog_title,
        resDialogText = R.string.crash_dialog_text)
public class MyApplication extends Application {

    public MyApplication() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        ACRA.init(this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                DbHelper.getInstance().openDb(getApplicationContext());
            }
        }).start();

    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        new Thread(new Runnable() {
            @Override
            public void run() {
                DbHelper.getInstance().closeDb();
            }
        }).start();


    }
}
