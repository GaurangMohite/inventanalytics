/*
 * Copyright (c) 2015. IdeasBanq PTE LTD. All rights reserved. This material may not be reproduced, displayed, modified or distributed without the express prior written permission of the copyright holder. For permission, contact Dr Insu Song, inssong@gmail.com.
 */

package com.ideasbanq.androidtemplate.Model;

import java.util.Date;

public class BaseFolderFile {

    public int mFId;
    public String mName;
    public int mUserId;
    public String mDescription;
    public Date mUpdate;
    public String mNote;
    public int mParentFolderId;

    public int mShareLink;
    public Date mUdate;
    public Date mUtime;

    /*file attributes*/
    public Boolean isFile;
    public int mFileSize;
    public String mExt;
}
