/*
 * Copyright (c) 2015. IdeasBanq PTE LTD. All rights reserved. This material may not be reproduced, displayed, modified or distributed without the express prior written permission of the copyright holder. For permission, contact Dr Insu Song, inssong@gmail.com.
 */

package com.ideasbanq.androidtemplate;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.ideasbanq.androidtemplate.Fragments.OnFragmentInteractionListener;

import com.ideasbanq.androidtemplate.Fragments.AnalysisFragment;
import com.ideasbanq.androidtemplate.Fragments.AnalysisListFragment;
import com.ideasbanq.androidtemplate.Fragments.ViewEditFragment;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,OnFragmentInteractionListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;


    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private final static String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    public void onAdd(View view)
    {
      // System.out.println("Add");
        onNavigationDrawerItemSelected(1);

    }
    public void onEdit(View view)
    {
        onNavigationDrawerItemSelected(2);

    }
    public void onView(View view)
    {
        onNavigationDrawerItemSelected(2);

    }
    public void onDelete(View view)
    {
      EditText editText1 = (EditText)findViewById(R.id.editText);
      EditText editText2 = (EditText)findViewById(R.id.editText2);
      editText1.setText("");
      editText2.setText("");
    }
    public void onCancel(View view)
    {
        onNavigationDrawerItemSelected(0);
    }
    public void onCancel1(View view)
    {
        onNavigationDrawerItemSelected(0);
    }
    @Override
    /**
     *  Handle selection of a page in the vertical Menu bar that slide out
     */
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        Log.d(TAG, "onNavigationDrawerItemSelected: drawer selected: pos=" + position);

        if (position == 0) { // first menu item
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new AnalysisListFragment())
                    .commit();
        } else if (position == 1) { // second menu item
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new AnalysisFragment())
                    .commit();
        }
        else if (position == 2) { // third menu item
            System.out.println("===============================================================");
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new ViewEditFragment())
                    .commit();
        }
    }

    /**
     * Change the title of the page
     * @param number
     */
    public void onSectionAttached(int number) {
        Log.d(TAG, "onSectionAttached: " + number);
        mTitle = "Page1";
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);

        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setTitle(mTitle);
    }

    /**
     * The action menu shows on the right when clicked the option button.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu: ");

        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    /**
     *  Handle Option item selection here
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Log.d(TAG, "onOptionsItemSelected: " + item.getTitle() + " id=" + id + ", R.id=" + R.id.action_settings);

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Log.d(TAG, "onOptionsItemSelected: selected settings");
            return true;
        }
        else if (id == R.id.action_main_search) {
            Log.d(TAG, "onOptionsItemSelected: selected search");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    /**
     *  Handle events of current fragment
     */
    public void onFragmentInteraction(Uri uri) {
        Log.d(TAG, "onFragmentInteraction: ---------" + uri);

    }

}
