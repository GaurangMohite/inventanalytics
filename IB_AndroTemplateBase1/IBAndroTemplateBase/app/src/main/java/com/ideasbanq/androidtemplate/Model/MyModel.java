/*
 * Copyright (c) 2015. IdeasBanq PTE LTD. All rights reserved. This material may not be reproduced, displayed, modified or distributed without the express prior written permission of the copyright holder. For permission, contact Dr Insu Song, inssong@gmail.com.
 */

package com.ideasbanq.androidtemplate.Model;

public class MyModel {

    public final MyFile mFile;
    public final MyFolder mFolder;
    public final Boolean isFile;

    public MyModel(MyFile mFile, MyFolder mFolder, Boolean isFile) {
        this.mFile = mFile;
        this.mFolder = mFolder;
        this.isFile = isFile;
    }
}
