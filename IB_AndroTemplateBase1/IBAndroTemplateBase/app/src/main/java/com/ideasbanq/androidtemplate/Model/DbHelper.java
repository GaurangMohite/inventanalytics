/*
 * Copyright (c) 2015. IdeasBanq PTE LTD. All rights reserved. This material may not be reproduced, displayed, modified or distributed without the express prior written permission of the copyright holder. For permission, contact Dr Insu Song, inssong@gmail.com.
 */

package com.ideasbanq.androidtemplate.Model;

import android.content.Context;
import android.os.Environment;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;
import com.ideasbanq.androidtemplate.MyPrefrence;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DbHelper {

    String dbpath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/mydb";
    ObjectContainer db;
    private static DbHelper instance;


    private static final int BASE_ID = 1000;


    public static DbHelper getInstance() {

        if (instance == null) {
            instance = new DbHelper();
        }

        return instance;
    }

    public void deleteDb() {

        File file = new File(dbpath);

        if (file.exists()) {
            file.delete();
        }

    }

    public void openDb(Context context) {

        String dbpath = context.getDir("data", 0) + "/" + "pumpup.db4o";
        // String dbpath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/mydb";
        db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), dbpath);
    }

    public void closeDb() {
        if (db != null) {
            db.close();
        }
    }

    /**
     * @param fileFolder
     * @param parent
     * @return id of created folder
     */
    public int addFileFolder(Context context, BaseFolderFile fileFolder, BaseFolderFile parent) {

        if (db == null) {
            throw new RuntimeException("addFileFolder: db not initalized");
        }

        int id = MyPrefrence.getNextId(context);
        fileFolder.mFId = id;

        if (parent == null) {
            fileFolder.mParentFolderId = 0; //set parent
        } else {
            fileFolder.mParentFolderId = parent.mFId; //set parent
        }


        db.store(fileFolder);
        db.commit();

        return id;
    }

    public int addFileFolder(Context context, BaseFolderFile fileFolder, int parentId) {

        if (db == null) {
            throw new RuntimeException("addFileFolder: db not initalized");
        }

        int id = MyPrefrence.getNextId(context);
        fileFolder.mFId = id;

        fileFolder.mParentFolderId = parentId; //set parent

        db.store(fileFolder);
        db.commit();

        return id;
    }

    /**
     * @param parent null for home folder
     * @return directory listings
     */
    public List<BaseFolderFile> getChildren(final BaseFolderFile parent) {

        ObjectSet<BaseFolderFile> result = db.query(new Predicate<BaseFolderFile>() {
            @Override
            public boolean match(BaseFolderFile o) {

                if (parent == null) {
                    return (o.mParentFolderId == 0);
                } else {
                    return (o.mParentFolderId == parent.mFId);
                }
            }
        });

        List<BaseFolderFile> directoryList = new ArrayList<BaseFolderFile>();
        if (result != null) {
            while (result.hasNext()) {
                directoryList.add(result.next());
            }
        }
        return directoryList;
    }


    public BaseFolderFile getFolder(final int fid) {
        ObjectSet<BaseFolderFile> result = db.query(new Predicate<BaseFolderFile>() {
            @Override
            public boolean match(BaseFolderFile o) {

                if (fid == 0) {
                    return (o.mParentFolderId == 0);
                } else {

                    return (o.mFId == fid);
                }
            }
        });

        if (result.hasNext()) {
            return result.next();
        } else {
            return null;
        }
    }

    public void moveFileFolder(BaseFolderFile fileFolder, BaseFolderFile newParent) {


        ObjectSet result = db.queryByExample(fileFolder);

        if (result.hasNext()) {

            BaseFolderFile object = (BaseFolderFile) result.next();

            if (newParent == null) {
                object.mParentFolderId = 0;
            } else {
                object.mParentFolderId = newParent.mFId;
            }


            db.store(object);
        }

    }

    public void deleteFileFolder(BaseFolderFile obj) {

        ObjectSet result = db.queryByExample(obj);

        if (result.hasNext()) {
            db.delete(result.next());
        }
    }

    public void renameFolder(BaseFolderFile fileFolder, String newName) {

        ObjectSet result = db.queryByExample(fileFolder);

        if (result.hasNext()) {

            BaseFolderFile obj = (BaseFolderFile) result.next();
            obj.mName = newName;

            db.store(obj);
        }

    }
}
