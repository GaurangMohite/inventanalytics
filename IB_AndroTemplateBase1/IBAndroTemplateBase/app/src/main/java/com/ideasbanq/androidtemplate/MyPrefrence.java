/*
 * Copyright (c) 2015. IdeasBanq PTE LTD. All rights reserved. This material may not be reproduced, displayed, modified or distributed without the express prior written permission of the copyright holder. For permission, contact Dr Insu Song, inssong@gmail.com.
 */

package com.ideasbanq.androidtemplate;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import java.io.File;

public class MyPrefrence {

    private static final String KEY_FIRST_LAUNCH = "first_launch";
    private static final String KEY_NEXT_ID = "next_id";

    public static boolean isFirstLaunch(Context context) {

        if (context == null) {
            throw new RuntimeException("isFirstLaunch: context cannot be null");
        }

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(KEY_FIRST_LAUNCH, true);

    }

    public static void setFirstLaunch(Context context, boolean isFirst) {

        if (context == null) {
            throw new RuntimeException("setFirstLaunch: context cannot be null");
        }

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        sp.edit().putBoolean(KEY_FIRST_LAUNCH, isFirst).commit();
    }

    public static int getNextId(Context context) {
        if (context == null) {
            throw new RuntimeException("setFirstLaunch: context cannot be null");
        }

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        int id = sp.getInt(KEY_NEXT_ID, 1000);

        sp.edit().putInt(KEY_NEXT_ID, id + 1).commit();

        return id;

    }

    public static File getAppFolder(Context context){

        if(context==null){
            throw new RuntimeException("getAppFolder: context can't be null");
        }

        String path= Environment.getExternalStorageDirectory()+"/Android/data/"+context.getPackageName()+"/files/";

        File directory=new File(path);

        if(!directory.exists()){
            directory.mkdirs();
        }

        return directory;
    }


}
